#!/usr/bin/python3

import json
from http.server import BaseHTTPRequestHandler
import socketserver
from urllib.parse import urlparse, parse_qs
import sqlite3
import html

conf = json.load(open('config.json', 'rb'))

BASE62_LIST = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
def base62_encode(num: int):
	s = ''
	while num > 0:
		num, r = divmod(num, 62)
		s = BASE62_LIST[r] + s
	return s


con = sqlite3.connect('ads.db')
con.row_factory = sqlite3.Row
cur = con.cursor()
cur.execute('CREATE TABLE IF NOT EXISTS comments(id INT PRIMARY KEY, cmt TEXT)')

class HttpHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		if self.path == '/':
			self.send_response(200)
			self.send_header('Content-type', 'text/html; charset=utf-8')
			self.end_headers()
			self.wfile.write(b'''
<!DOCTYPE html>
<base target="_blank">
<style>
div {
	width: 100%;
	float: left;
	border-bottom: 2px solid black;
}
img {
	float: left;
	margin: 0 15px 8px 0;
	width: 200px;
}
span {
	float: right;
}
input {
	color: red;
	font-weight: bold;
}
</style>
<script>
var comment = function(id, cmt = document.getElementsByName(`cmt_${id}`)[0].value) {
	fetch(new Request(`/comment?id=${id}&cmt=${encodeURIComponent(cmt)}`)).then((res) => {
		if(res.ok) if (cmt == 'x') {
			document.getElementById(`ad_${id}`).remove()
		} else {
			document.getElementsByName(`cmt_${id}`)[0].value = cmt
		}
	})
}
</script>
''')
			for ad in cur.execute('SELECT * FROM ads NATURAL LEFT JOIN comments NATURAL LEFT JOIN prices WHERE cmt IS NOT "x" ORDER BY price'):
				id = ad['id']
				price = ad['price']
				price_old = f'<s>{ad["price_old"]}</s> ' if ad['price_old'] != price else ''
				img = ad['img']
				title = ad['title']
				txt = ad['txt'].replace('<br />', ' ')
				cmt = html.escape(ad['cmt']) if ad['cmt'] else ''
				self.wfile.write(f'''
<div id="ad_{id}">
<a href="https://www.olx.ua/d/uk/obyavlenie/-ID{base62_encode(id)}.html">
<img src="https://ireland.apollo.olxcdn.com/v1/files/{img}-UA/image;s=200x0;q=50">
{price_old}<b>{price} грн</b> {title}
</a>
<span>
<input name="cmt_{id}" value="{cmt}">
<button onclick="comment({id})">&#128190;</button>
<button onclick="comment({id}, '+')">+</button>
<button onclick="comment({id}, '-')">-</button>
<button onclick="comment({id}, 'x')">x</button>
</span>
<br>
{txt}
</div>
'''.encode('utf-8'))
		elif self.path.startswith('/comment'):
			query = parse_qs(urlparse(self.path).query, keep_blank_values=True)
			id = query['id'][0]
			cmt = query['cmt'][0]
			cur.execute('INSERT INTO comments VALUES(?, ?) ON CONFLICT DO UPDATE SET cmt=? WHERE id=?', [id, cmt, cmt, id])
			con.commit()
			self.send_response(200)
			self.end_headers()
		else:
			self.send_error(404)

with socketserver.TCPServer(tuple(conf['server']), HttpHandler) as httpd:
	httpd.allow_reuse_address = True
	httpd.serve_forever()
