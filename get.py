#!/usr/bin/python3

import json
import requests
import re
import codecs
import sqlite3

conf = json.load(open('config.json', 'rb'))

session = requests.Session()

con = sqlite3.connect('ads.db')
cur = con.cursor()
cur.executescript('''
	CREATE TABLE IF NOT EXISTS ads(id INT PRIMARY KEY, price INT, img TEXT, title TEXT, txt TEXT);
	CREATE TABLE IF NOT EXISTS prices(id INT PRIMARY KEY, price_old INT);
	DELETE FROM ads;
''')

for q_dict in conf['queries']:
	category = q_dict.get('category') or 'list'
	query = q_dict['query'].replace(' ', '-')
	price_min = q_dict['price_min']
	price_max = q_dict['price_max']
	page = 1
	price = None
	while True:
		url = f'https://www.olx.ua/d/uk/{category}/q-{query}/?search[filter_float_price:from]={price_min}&search[order]=filter_float_price:asc&page={page}'
		print(url)
		res = session.get(url)
		res.raise_for_status()
		js_str = re.search(r'^ {8}window.__PRERENDERED_STATE__= "(.+)";$', res.text, flags=re.MULTILINE)[1]
		js_obj = json.loads( codecs.escape_decode(js_str)[0] )
		for ad in js_obj['listing']['listing']['ads']:
			price = ad['price']['regularPrice']['value']
			if price > price_max:
				continue
			id = ad['id']
			img = re.search(r'([^/]+)-', ad['photos'][0] )[1] if ad['photos'] else None
			title = ad['title']
			txt = ad['description']
			cur.execute('INSERT OR IGNORE INTO ads VALUES(?, ?, ?, ?, ?)', [id, price, img, title, txt])
			cur.execute('INSERT OR IGNORE INTO prices VALUES(?, ?)', [id, price])
		if price > price_max:
			break
		if page < 25:
			page += 1
		else:
			page = 1
			price_min = price
cur.execute('DELETE FROM prices WHERE id IN (SELECT prices.id FROM prices NATURAL LEFT JOIN ads WHERE ads.id IS NULL)')
con.commit()
